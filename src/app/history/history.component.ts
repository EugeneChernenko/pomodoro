import { Component, OnInit, OnDestroy } from '@angular/core';

import { Subscription } from "rxjs";

import { TasksApiService } from '../tasks-api.service';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponent implements OnInit {
  private taskSubscription: Subscription;
  private tasksByDay: {} = {};
  private totalPomodoro: number = 0;

  constructor(private tasksApiService: TasksApiService) {}

  ngOnInit() {
    this.tasksApiService.getByDay();
    this.taskSubscription = this.tasksApiService.tasksByDay$.subscribe(tasks => {
      this.tasksByDay = tasks
    });
  }


  private get historyKeys() {
    return this.tasksByDay ? Object.keys(this.tasksByDay).sort(this.compareByDay.bind(this)).reverse() : []
  }


  private countTotalDailyPomodoro(day: {}): number {
    return Object.keys(day)
      .reduce((totalPomodoro, task) => totalPomodoro + day[task].pomodoro, 0)
  }

  private compareByDay(a, b): number {
    return this.tasksByDay[a][0].completeDate - this.tasksByDay[b][0].completeDate;
  }

  private isEmptyObject(obj) {
    return (obj && (Object.keys(obj).length === 0));
  }
}
