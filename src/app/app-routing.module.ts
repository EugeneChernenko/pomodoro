import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TimerComponent } from './timer/timer.component';
import { StatsComponent } from './stats/stats.component';
import { HistoryComponent } from './history/history.component';

const routes: Routes = [
	{ path: '', redirectTo: '/timer', pathMatch: 'full' },
	{ path: 'timer', component: TimerComponent },
	{ path: 'stats', component: StatsComponent },
	{ path: 'history', component: HistoryComponent }
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})

export class AppRoutingModule { }
