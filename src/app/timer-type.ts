export enum TimerType {
  POMODORO = 'pomodoro',
  BREAK = 'break'
}
