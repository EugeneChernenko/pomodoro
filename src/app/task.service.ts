import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Subscription } from 'rxjs';

import { Task } from './task';
import { TimerType } from './timer-type';
import { TaskStatusType } from './task-status-type';

import { TimerService } from './timer.service';
import { NotificationService } from './notification.service';
import { TasksApiService } from './tasks-api.service';

@Injectable()
export class TaskService {
  private tasks: Task[];
  private taskToggling = new Subject < any > ();
  private activeTaskExist = new Subject < any > ();
  private TimerType = TimerType;
  private TaskStatusType = TaskStatusType;
  private completedPomodoroSubscription: Subscription;
  private taskSubscription: Subscription;

  public activeTask: Task;

  public taskToggling$ = this.taskToggling.asObservable();
  public activeTaskExist$ = this.activeTaskExist.asObservable();

  constructor(private timerService: TimerService, private notificationService: NotificationService, private tasksApiService: TasksApiService) {
    this.completedPomodoroSubscription = this.timerService.finished$.subscribe(payload =>
      this.publishCompletedPomodoro(payload));

    this.taskSubscription = this.tasksApiService.tasks$.subscribe(tasks => {
      this.tasks = tasks;
      this.checkActiveTask();
    });
  }
  checkActiveTask(): void {
    this.tasks.forEach(task => {
      if (task.status == this.TaskStatusType.IN_PROGRESS) {
        this.activeTask = task;
        this.activeTaskExist.next(this.activeTask);
      }
    })
  }
  publishCompletedPomodoro(payload): void {
    if (payload.type == this.TimerType.POMODORO) {
      this.addCompletedPomodoro();
      this.notify('Pomodoro completed!');
    }
  }

  addCompletedPomodoro(): void {
    this.activeTask.pomodoro++;
    this.tasksApiService.edit(this.TimerType.POMODORO, this.activeTask.pomodoro, this.activeTask.id).subscribe();
  }

  toggleTask(task: Task): void {
    if (this.timerService.isStarted()) {
      this.notify('Error! Please stop your timer or wait until it is finished!')
    } else {
      if (task.status == this.TaskStatusType.DONE) {
        this.notify('Error! You can\'t start working on closed task. Please reopen it before starting!');
        return
      }
      if (task.status !== this.TaskStatusType.IN_PROGRESS) {
        this.deactivatePreviousTask();
        this.activateCurrentTask(task);
      } else {
        this.deactivatePreviousTask();
      }
      this.taskToggling.next(task);
    }
  }

  deactivatePreviousTask(): void {
    if (this.activeTask) {
      this.activeTask.status = this.TaskStatusType.OPEN;
      this.tasksApiService.edit('status', this.TaskStatusType.OPEN, this.activeTask.id).subscribe();
      this.activeTask = null;
    }
  }

  activateCurrentTask(task: Task): void {
    task.status = this.TaskStatusType.IN_PROGRESS;
    this.tasksApiService.edit('status', this.TaskStatusType.IN_PROGRESS, task.id).subscribe();
    this.activeTask = task;
  }

  private notify(message: string) {
    this.notificationService.add(message);
  }
}
