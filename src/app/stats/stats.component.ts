import { Component, OnInit, OnDestroy } from '@angular/core';

import { Subscription } from "rxjs";

import { Task } from '../task';
import { TaskStatusType } from '../task-status-type';

import { TasksApiService } from '../tasks-api.service';

const MONTH_IN_MILLISECONDS = 30 * 24 * 60 * 60 * 1000;

@Component({
  selector: 'app-stats',
  templateUrl: './stats.component.html',
  styleUrls: ['./stats.component.css']
})
export class StatsComponent implements OnInit {
  private tasks: Task[];
  private taskSubscription: Subscription;
  private tasksByPomodoroSubscription: Subscription;
  private pomodoroPerTask: number = 0;
  private pomodoroPerDay: number = 0;
  private doneTasksAmount: number = 0;
  private totalPomodoroAmount: number = 0;
  private tasksByPomodoro: {} = {};
  private topTasks: {} = {};
  private readonly topTasksAmount: number = 2;

  constructor(private tasksApiService: TasksApiService) {}

  ngOnInit() {
    this.tasksApiService.getAll();
    this.tasksApiService.getByPomodoro();
    this.taskSubscription = this.tasksApiService.tasks$.subscribe(tasks => {
      this.tasks = tasks;
      this.countStatistics();
    });
    this.tasksByPomodoroSubscription = this.tasksApiService.tasksByPomodoro$.subscribe(tasks => {
      this.tasksByPomodoro = tasks
      this.getTopTasks(this.tasksByPomodoro);
    });
  }

  private getTopTasks(tasks: {}): any {
    Object.keys(tasks).forEach(key => {
      // Skip tasks with 0 pomodoros
      if (key === '0') { return }

      this.topTasks[key] = tasks[key].slice(0, this.topTasksAmount);
    });
    return this.topTasks;
  }

  private get topTasksKeys() {
    return this.topTasks ? Object.keys(this.topTasks).sort().reverse() : []
  }

  private countStatistics(): void {
    if (this.tasks.length < 1) { return }
    this.countPomodoroPerTask();
    this.countPomodoroPerDay();
  }

  private countDoneTasks(): number {
    return this.tasks
      .filter(task => task.status === TaskStatusType.DONE)
      .length;
  }

  private countTotalPomodoro(): number {
    return this.tasks
      .filter(task => this.isDone(task) && this.hasPomodoro(task))
      .reduce((totalPomodoroAmount, task) => totalPomodoroAmount + task.pomodoro, 0)
  }

  private isDone(task: Task): boolean {
    return task.status === TaskStatusType.DONE;
  }

  private hasPomodoro(task: Task): boolean {
    return task.pomodoro > 0;
  }

  countPomodoroPerTask(): void {
    let doneTasks: number = this.countDoneTasks();
    let totalPomodoro: number = this.countTotalPomodoro();
    this.pomodoroPerTask = parseFloat((totalPomodoro / doneTasks).toFixed(2));
  }

  countPomodoroPerDay(): void {
    let monthPomodoroTasks = this.getMonthPomodoroTasks();
    let monthPomodoroAmount = this.countMonthPomodoroAmount(monthPomodoroTasks);
    let daysAmount = this.countDaysAmount(monthPomodoroTasks);
    this.pomodoroPerDay = parseFloat((monthPomodoroAmount / daysAmount).toFixed(2));
  }

  private countMonthPomodoroAmount(tasks: Task[]): number {
    let monthPomodoroAmount: number = 0;
    tasks.forEach(task => {
      monthPomodoroAmount = monthPomodoroAmount + task.pomodoro;
    })
    return monthPomodoroAmount;
  }

  private convertToDay(val: number): number {
    return new Date(val).getDate();
  }

  private countDaysAmount(tasks: Task[]): number {
    let daysAmount: number = 1;

    for (let i = 1; i < tasks.length; i++) {
      if (this.convertToDay(tasks[i].completeDate) !== this.convertToDay(tasks[i - 1].completeDate)) {
        daysAmount++;
      }
    }
    return daysAmount;
  }

  private getMonthPomodoroTasks(): Task[] {
    let today = +new Date();
    return this.tasks
      .filter(task => this.hasPomodoro(task) && (today - task.completeDate) < MONTH_IN_MILLISECONDS)
  }

  ngOnDestroy() {
    this.taskSubscription.unsubscribe();
  }
}
