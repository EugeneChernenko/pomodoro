import { Component, OnInit, OnDestroy } from '@angular/core';

import { Subscription } from "rxjs";

import { Task } from '../task';
import { TaskStatusType } from '../task-status-type';

import { TaskService } from '../task.service';
import { TasksApiService } from '../tasks-api.service';
import { NotificationService } from '../notification.service';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.css']
})
export class TodoListComponent implements OnInit {
  private tasks: Task[];
  private taskSubscription: Subscription;
  private isCreateTask: boolean = false;
  private isEditTask: boolean = false;
  private editedTask: Task;
  private TaskStatusType = TaskStatusType;

  constructor(private notificationService: NotificationService, private taskService: TaskService, private tasksApiService: TasksApiService) {

  }

  ngOnInit() {
    this.taskSubscription = this.tasksApiService.tasks$.subscribe(tasks => {
      this.tasks = tasks;
      });

    this.tasksApiService.getAll();
  }

  toggleTask(task: Task): void {
    this.taskService.toggleTask(task);
  }

  add(taskTitle: string): any {
    if (!this.IsEmpty(taskTitle)) { return }
    let newTask = {
      title: taskTitle.toString(),
      status: this.TaskStatusType.OPEN,
      pomodoro: 0,
      completeDate: null
    }
    this.tasksApiService.add(newTask).subscribe(task => {
      this.tasks.push(task);
    });
    this.isCreateTask = false;
  }

  edit(taskTitle: string): any {
    if (!this.IsEmpty(taskTitle)) { return }
    this.tasks.forEach(task => {
      if (task.title == this.editedTask.title) {
        task.title = taskTitle.toString();
      }
    })
    this.tasksApiService.edit('title', taskTitle, this.editedTask.id).subscribe();
    this.isEditTask = false;
  }

  delete(removedTask: Task): void {
    if (this.isActive(removedTask)) {
      this.notify('Error! You can\'t delete active task. Please stop the task before deleting!');
      return
    }
    this.tasks = this.tasks.filter(task => task !== removedTask);
    this.tasksApiService.delete(removedTask).subscribe();
  }

  done(task: Task) {
    if (this.isActive(task)) {
      this.notify('Error! You can\'t close active task. Please stop the task before closing!');
      return
    }
    if (task.status == this.TaskStatusType.DONE) {
      this.reopen(task);
    } else {
      task.status = this.TaskStatusType.DONE;
      this.tasksApiService.edit('status', task.status, task.id).subscribe();
      this.tasksApiService.edit('completeDate', new Date().getTime(), task.id).subscribe();
    }
  }

  private reopen(task: Task) {
    task.status = this.TaskStatusType.OPEN;
    this.tasksApiService.edit('status', task.status, task.id).subscribe();
  }

  private IsEmpty(input: any): boolean {
    return input = input.trim();
  }

  private notify(message: string) {
    this.notificationService.add(message);
  }
  private isActive(task: Task): boolean {
    return task == this.taskService.activeTask;
  }
  ngOnDestroy() {
    this.taskSubscription.unsubscribe();
  }
}
