import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'millisecondsToDate'
})
export class MillisecondsToDatePipe implements PipeTransform {
  transform(ms: number): string {
  	let date = new Date(ms);
    return `${date.getDate()}/${date.getMonth()}/${date.getFullYear()}`
  }
}
