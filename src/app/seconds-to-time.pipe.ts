import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
	name: 'secondsToTime'
})
export class SecondsToTimePipe implements PipeTransform {
	transform(totalSeconds: number): string {
		let hours = Math.floor(totalSeconds / 3600);
		totalSeconds %= 3600;
		let minutes: any = Math.floor(totalSeconds / 60);
		let seconds: any = totalSeconds % 60;
		minutes = minutes < 10 ? `0${minutes}` : minutes;
		seconds = seconds < 10 ? `0${seconds}` : seconds;
		return `${minutes}:${seconds}`
	}
}