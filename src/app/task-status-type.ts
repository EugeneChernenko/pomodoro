export enum TaskStatusType {
  OPEN = 'open',
  IN_PROGRESS = 'in-progress',
  DONE = 'done'
}
