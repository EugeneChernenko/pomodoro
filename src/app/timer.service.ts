import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { Subscription } from "rxjs";
import { TimerObservable } from "rxjs/observable/TimerObservable";
import { Subject } from 'rxjs/Subject';

import { NotificationService } from './notification.service';

let toSeconds = function(mins: number): number {
  return mins * 60;
};

let toMinutes = function(seconds: number): number {
  return seconds / 60;
};

@Injectable()
export class TimerService {
  private TIMER_INITIAL_POINT = 0;
  private TIMER_STEP = 1000;
  private tick = new Subject < any > ();
  private finished = new Subject < any > ();
  private subscription: Subscription;
  private timeLeft: number = 0;
  private paused: boolean = false;

  public tick$ = this.tick.asObservable();
  public finished$ = this.finished.asObservable();

  constructor(private notificationService: NotificationService) {

  }

  start(mins: number, payload ? : Object): void {
    if (this.subscription) {
      this.unsubscribe();
    }

    let timer = TimerObservable.create(this.TIMER_INITIAL_POINT, this.TIMER_STEP);
    this.subscription = timer.subscribe(count => {
      this.timeLeft = toSeconds(mins) - count;
      if (this.timeLeft <= 0) {
        this.finished.next(payload);
        this.unsubscribe();
      }
      this.tick.next(this.timeLeft);
    })
  }

  pause(): void {
    this.paused = true;
    this.unsubscribe();
  }

  stop(): void {
    this.timeLeft = 0;
    this.paused = false;
    this.unsubscribe();
  }

  resume(): void {
    this.paused = false;
    let mins = toMinutes(this.timeLeft);
    return this.start(mins);
  }

  isPaused(): boolean {
    return this.paused;
  }

  isStarted(): boolean {
    return this.timeLeft > 0;
  }

  getTimeLeft(): number {
    return this.timeLeft;
  }

  private unsubscribe(): void {
    this.subscription.unsubscribe();
  }

  private notify(message: string) {
    this.notificationService.add(message);
  }
}
