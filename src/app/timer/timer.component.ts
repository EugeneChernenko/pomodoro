import { Component, OnInit, OnDestroy } from '@angular/core';

import { Subscription } from "rxjs";

import { TimerType } from '../timer-type';
import { TimerService } from '../timer.service';
import { TaskService } from '../task.service';
import { NotificationService } from '../notification.service';

@Component({
  selector: 'app-timer',
  templateUrl: './timer.component.html',
  styleUrls: ['./timer.component.css']
})
export class TimerComponent implements OnInit {
  private TimerType = TimerType;
  private tickSubscription: Subscription;
  private timeleft: number = 0;
  private timerStarted: boolean = false;

  constructor(private timerService: TimerService, private taskService: TaskService, private notificationService: NotificationService) {

  }

  ngOnInit() {
    this.timerStarted = this.timerService.isStarted();

    if (this.timerStarted) {
      this.timeleft = this.timerService.getTimeLeft();
    }

    this.tickSubscription = this.timerService.tick$.subscribe(time => this.onTimerTick(time));
  }

  private onTimerTick(time) {
    this.timeleft = time;
  }

  startTimer(mins: number, payload ? : Object): void {
    if (this.taskService.activeTask) {
      this.timerStarted = true;
      this.timerService.start(mins, payload);
    } else {
      this.notify('You don\'t have any active tasks! Please start the task before your timer!');
    }
  }

  pauseTimer(): void {
    this.timerService.pause();
  }

  stopTimer(): void {
    this.timerService.stop();
    this.timeleft = 0;
    this.timerStarted= false;
  }

  resumeTimer(): void {
    if (this.isPaused()) {
      this.timerService.resume();
    }
  }

  private isPaused(): boolean {
    return this.timerService.isPaused();
  }

  ngOnDestroy() {
    this.tickSubscription.unsubscribe();
  }

  private notify(message: string) {
    this.notificationService.add(message);
  }
}
