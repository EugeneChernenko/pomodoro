import { Component, OnInit, OnDestroy } from '@angular/core';

import { Subscription } from "rxjs";

import { NotificationService } from '../notification.service';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.css']
})
export class NotificationsComponent implements OnInit {
  private notifications: string[] = [];
  private notificationsSubscription: Subscription;

  constructor(public notificationService: NotificationService) {}

  ngOnInit() {
    this.notificationsSubscription = this.notificationService.notifications$.subscribe(message => {
      this.notifications.push(message);
      this.clear();
    });
  }

  clear(): void {
    setTimeout(() => this.notifications.shift(), 6000);
  }

  ngOnDestroy() {
    this.notificationsSubscription.unsubscribe();
  }
}
