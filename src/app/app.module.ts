import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './/app-routing.module';

import { AppComponent } from './app.component';
import { TimerComponent } from './timer/timer.component';
import { TodoListComponent } from './todo-list/todo-list.component';
import { TaskService } from './task.service';
import { TimerService } from './timer.service';
import { SecondsToTimePipe } from './seconds-to-time.pipe';
import { MillisecondsToDatePipe } from './milliseconds-to-date.pipe';
import { ActiveTaskComponent } from './active-task/active-task.component';
import { StatsComponent } from './stats/stats.component';
import { NotificationService } from './notification.service';
import { NotificationsComponent } from './notifications/notifications.component';
import { TasksApiService } from './tasks-api.service';
import { HistoryComponent } from './history/history.component';

@NgModule({
  declarations: [
    AppComponent,
    TimerComponent,
    TodoListComponent,
	SecondsToTimePipe,
  MillisecondsToDatePipe,
	ActiveTaskComponent,
	StatsComponent,
	NotificationsComponent,
	HistoryComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [TaskService, TimerService, NotificationService, TasksApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
