import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';
import { of } from 'rxjs/observable/of';

import { Task } from './task';

const INDEXED_DB_VERSION = 3;

let handleRequest = request => {
  return new Promise((resolve, reject) => {
    request.onsuccess = event => {
      resolve(event.target);
    }
    request.onerror = event => {
      reject(event.target);
    };
  })
};

let handleCollectionRequest = request => {
  let result = [];
  return new Promise((resolve, reject) => {
    request.onsuccess = event => {

      let cursor = event.target.result;

      if (!cursor) {
        return resolve(result);
      }

      result.push({ key: cursor.primaryKey, value: cursor.value });
      cursor.continue();
    }

    request.onerror = event => {
      reject(event.target);
    }
  })
};

@Injectable()
export class TasksApiService {
  private taskDb: string = 'TaskDb';
  private tasksDbName: string = 'tasks';
  private tasks = new Subject < any > ();
  private tasksByPomodoro = new Subject < any > ();
  private tasksByDay = new Subject < any > ();
  private dbIsReady;

  public tasks$ = this.tasks.asObservable();
  public tasksByPomodoro$ = this.tasksByPomodoro.asObservable();
  public tasksByDay$ = this.tasksByDay.asObservable();

  constructor() {
    this.dBInit();
  }

  private dBInit(): void {
    this.dbIsReady = new Promise((resolve, reject) => {
      let request = window.indexedDB.open(this.taskDb, INDEXED_DB_VERSION);

      handleRequest(request)
        .then((data: any) => {
          console.log('success: ');
          resolve(data.result);
        })
        .catch(() => {
          console.error('Unable get tasks');
        })

      request.onupgradeneeded = event => {
        let target: any = event.target;
        let db = target.result;
        let objectStore = db.createObjectStore(this.tasksDbName, { keyPath: 'id', autoIncrement: true });
        let pomodoroIndex = objectStore.createIndex('by_pomodoro', 'pomodoro', { unique: false });
        let dailyIndex = objectStore.createIndex('by_day', 'completeDate', { unique: false });
      }
    })
  }

  public getAll(): void {
    this.dbIsReady.then(db => {
      let objectStore = db.transaction(this.tasksDbName)
        .objectStore(this.tasksDbName)
        .openCursor();

      handleCollectionRequest(objectStore)
        .then((result:Array<any>) => {
          let taskList = result.reduce((tasks, item) => {
            let task = item.value;
            tasks.push(task);

            return tasks;
          }, []);

          this.tasks.next(taskList);
        })
    })
  }

  public getByPomodoro(): void {
    this.dbIsReady.then(db => {
      let request = db.transaction(this.tasksDbName)
        .objectStore(this.tasksDbName)
        .index('by_pomodoro')
        .openCursor();

      handleCollectionRequest(request)
        .then((result:Array<any>) => {

          let samePomodoroTasks = result.reduce((tasks, item) => {
            let [pomodoro, task] = [item.value.pomodoro, item.value];
            tasks[pomodoro] = tasks[pomodoro] || []
            tasks[pomodoro].push(task);

            return tasks;
          }, {});

          this.tasksByPomodoro.next(samePomodoroTasks);
        })
    })
  }

  public getByDay(): void {
    this.dbIsReady.then(db => {
      let request = db.transaction(this.tasksDbName)
        .objectStore(this.tasksDbName)
        .index('by_day')
        .openCursor();

      handleCollectionRequest(request)
        .then((result:Array<any>) => {
          let keyToDay = key => new Date(+key).getDate();
          let tasksByDay = result.reduce((tasks, item) => {
            let [day, task] = [keyToDay(item.value.completeDate), item.value];
            tasks[day] = tasks[day] || []
            tasks[day].push(task);

            return tasks;
          }, {});

          this.tasksByDay.next(tasksByDay);
        })
    })
  }

  public add(task: any): Observable < Task > {
    this.dbIsReady.then(db => {
      let request = db.transaction([this.tasksDbName], 'readwrite')
        .objectStore(this.tasksDbName)
        .add(task);

      handleRequest(request)
        .then((data: any) => {
          console.log('Task has been added to your database.');
          task.id = data.result;
        })
        .catch(() => {
          console.error('Unable to add task');
        })
    })
    return of(task);
  }

  public delete(task: any): Observable < Task > {
    this.dbIsReady.then(db => {
      let request = db.transaction([this.tasksDbName], 'readwrite')
        .objectStore(this.tasksDbName)
        .delete(task.id);

      handleRequest(request)
        .then((data: any) => {
          console.log('Task has been deleted from your database.');
        })
        .catch(() => {
          console.error('Unable to delete the task!');
        })
    })
    return of(task);
  }

  public edit(property: string, value: any, id: number): Observable < string > {
    this.dbIsReady.then(db => {
      let objectStore = db.transaction([this.tasksDbName], 'readwrite')
        .objectStore(this.tasksDbName);

      let request = objectStore.get(id);

      handleRequest(request)
        .then((data: any) => {
          console.log('Task has been found in your database.');
          let task = data.result;
          task[property] = value;
          return objectStore.put(task);
        })
        .then(handleRequest)
        .then(() => {
          console.log('Task has been edited in your database.');
        })
        .catch(() => {
          console.log('Unable to edit the task! ');
        })
    })
    return of(property);
  }

}
