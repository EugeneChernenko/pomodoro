import { Injectable } from '@angular/core';


import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class NotificationService {
  private notifications = new Subject < any > ();

  private sound;

  public notifications$ = this.notifications.asObservable();

  constructor() {
    this.sound = new Audio();
    this.sound.src = "../assets/sounds/notification.mp3";
    this.sound.load();
  }

  add(message: string): void {
    this.notifications.next(message);
    this.playSound();
  }

  playSound(): void {
    this.sound.play();
  }

}
