import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';

import { TaskStatusType } from '../task-status-type';
import { Task } from '../task';

import { TaskService } from '../task.service';

@Component({
  selector: 'app-active-task',
  templateUrl: './active-task.component.html',
  styleUrls: ['./active-task.component.css']
})
export class ActiveTaskComponent {
  private activeTaskTitle: string = 'no active tasks';
  private activeTaskId: number;
  private taskStatus: string = 'Active task:';
  private TaskStatusType = TaskStatusType;

  taskTogglingSubscription: Subscription;
  activeTaskSubscription: Subscription;

  constructor(private taskService: TaskService) {
    this.activeTaskSubscription = this.taskService.activeTaskExist$.subscribe(task => {
      this.activeTaskId = task.id;
      this.activeTaskTitle = task.title;
    });

    this.taskTogglingSubscription = this.taskService.taskToggling$.subscribe(task => { this.taskToggling(task) })
  }

  ngOnInit() {

  }

  private taskToggling(task: Task):void {
    if (task.status == this.TaskStatusType.IN_PROGRESS) {
      this.activeTaskId = task.id;
      this.activeTaskTitle = task.title;
    } else if (task.status == this.TaskStatusType.OPEN) {
      this.activeTaskId = null;
      this.activeTaskTitle = 'no active tasks';
    }
  }
  ngOnDestroy() {
    if (this.taskTogglingSubscription) {
           this.taskTogglingSubscription.unsubscribe();
    }
    if (this.activeTaskSubscription)
      this.activeTaskSubscription.unsubscribe();
  }
}
